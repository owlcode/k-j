angular.module('fryzjernia')
    .controller('AppController', ['$scope',
        function ($scope) {
            var mv = $scope;

            $(document).ready(function () {
                var el = $('#main-slider');
                // var el = document.getElementById('main-slider');

                var _num = 0;
                var tab = ['assets/3.jpg', 'assets/main.jpg'];

                var fun = function () {

                    // el.setAttribute('data-parallax', 'scroll');
                    // el.setAttribute('data-image-src', tab[_num%2]);
                    _num++;
                    el.parallax({imageSrc: tab[_num % 2]});


                    setTimeout(fun, 1500);
                };
                fun();
            });


            // mv.main = parallaxHelper.createAnimator(-0.5);

        }]);