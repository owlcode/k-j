angular.module('fryzjernia', [
    'ngRoute'
]).config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'themes/fryzjernia.html',
        controller: 'AppController'
    }).otherwise({
        redirectTo: '/'
    });
}]);