<?php
class ClientController extends BaseController {
    public function all() {
        return Response::json(Client::all());
    }

    public function client($id) {
        return Response::json(Client::find($id));
    }

    public function delete($id) {
        try {
            $client = Client::find($id);
            $client->delete();

            $visits = Visit::where('klient_id', '=', $id)->delete();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie usunięto klienta !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }

    public function add() {
        try {
            $client = new Client;

            if(!empty(Input::get('imie')))
                $client->imie = Input::get('imie');
            else
                throw new Exception("Uzupełnij imię klienta !");

            if(!empty(Input::get('nazwisko')))
                $client->nazwisko = Input::get('nazwisko');
            else
                throw new Exception("Uzupełnij nazwisko klienta !");

            if(!empty(Input::get('telefon'))) $client->tel = Input::get('telefon');
            if(!empty(Input::get('mail'))) $client->mail = Input::get('mail');
            if(!empty(Input::get('adnotacje'))) $client->adnotacje = Input::get('adnotacje');

            $client->save();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie dodano klienta !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }

    public function edit($id) {
        try {
            $client = Client::find($id);

            if (!empty(Input::get('tel')))
            {
                $client->tel = Input::get('tel');
            }

            if(!empty(Input::get('adnotacje')))
            {
                $client->adnotacje = Input::get('adnotacje');
            }

            if(!empty(Input::get('mail')))
            {
                $client->mail = Input::get('mail');
            }

            $client->save();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie edytowano klienta !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }

    public function getPhoneNumber($id) {
        try {
            $client = Client::find($id);

            return Response::json(array(
                'name' => $client->imie.' '.$client->nazwisko,
                'phone' => $client->tel
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }
}