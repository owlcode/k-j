<?php
class VisitController extends BaseController {
    public function all() {
       return Response::json(Visit::all());
    }

    public function visit($id) {
        return Response::json(Visit::find($id));
    }

    public function userVisits($id) {
        try {

            $visit = Visit::where('klient_id', '=', $id)->orderBy('data', 'desc')->get();

            foreach ($visit as $val) {
                $val->stylista_id = $val->stylistName();
            }

            return Response::json($visit);

        } catch (Exception $e) {
            return Response::json($e->getMessage());
        }
    }

    public function delete($id) {
        try {
            Visit::destroy($id);

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie usunięto wizytę !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }

    public function add() {
        try {
            $visit = new Visit;

            $visit->stylista_id = Input::get('stylistid');
            $visit->klient_id = Input::get('clientid');
            $visit->data = Input::get('date');
            $visit->cena = Input::get('price');
            $visit->kk_nkw = Input::get('kk_nkw');
            $visit->kk_sikw = Input::get('kk_sikw');
            $visit->kk_us = Input::get('kk_us');
            $visit->kk_tw = Input::get('kk_tw');
            $visit->kk_akndik = Input::get('kk_akndik');
            $visit->kk_ze = Input::get('kk_ze');
            $visit->ppk_s = Input::get('ppk_s');
            $visit->ppk_ws = Input::get('ppk_ws');
            $visit->ppk_ppr = Input::get('ppk_ppr');
            $visit->ppk_ukr = Input::get('ppk_ukr');
            $visit->uk_rokb = Input::get('uk_rokb');
            $visit->uk_dik = Input::get('uk_dik');
            $visit->uk_tpr1 = Input::get('uk_tpr1');
            $visit->uk_tpr2 = Input::get('uk_tpr2');
            $visit->uk_tpr3 = Input::get('uk_tpr3');
            $visit->uk_tpcd = Input::get('uk_tpcd');
            $visit->uupk_s = Input::get('uupk_s');
            $visit->uupk_o = Input::get('uupk_o');
            $visit->uupk_sp = Input::get('uupk_sp');
            $visit->uupk_snf = Input::get('uupk_snf');
            $visit->uupk_su = Input::get('uupk_su');
            $visit->pkwd_sz = Input::get('pkwd_sz');
            $visit->pkwd_o = Input::get('pkwd_o');
            $visit->pkwd_st = Input::get('pkwd_st');
            $visit->pkwd_sw = Input::get('pkwd_sw');

            Stylist::where('id', '=', Input::get('stylistid'))->increment('wizyt');

            $visit->save();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie dodano wizytę !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }
}