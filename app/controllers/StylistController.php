<?php
class StylistController extends BaseController {
    public function all() {
        return Response::json(Stylist::all());
    }

    public function add() {
       try {
           $imie = Input::get('imie');
           $check = Stylist::where('nazwa', '=', $imie)->count();

           if($check != 0) throw new Exception("Istnieje stylista o takiej nazwie !");

           $stylist = new Stylist;

           $stylist->nazwa = Input::get('imie');
           $stylist->wizyt = 0;
           $stylist->save();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie dodano stylistę !'
            ));
       } catch (Exception $e) {
           return Response::json(array(
               'type' => 'ERROR',
               'msg' => $e->getMessage()
           ));
       }
    }

    public function delete($id) {
        try {
            $stylist = Stylist::find($id);

            $stylist->delete();

            return Response::json(array(
                'type' => 'SUCCESS',
                'msg' => 'Pomyślnie usunięto stylistę !'
            ));
        } catch (Exception $e) {
            return Response::json(array(
                'type' => 'ERROR',
                'msg' => $e->getMessage()
            ));
        }
    }
}