<?php
class Client extends Eloquent {

    protected $table = 'kj_klient';
    protected $hidden = array('tel');

    public $timestamps = false;

}