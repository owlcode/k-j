<?php

class Visit extends Eloquent {
    protected $table = 'kj_wizyta';
    public $timestamps = false;

    public function stylist() {
        return $this->belongsTo('Stylist', 'id', 'stylista_id');
    }

    public function stylistName() {
        $stylist = Stylist::where('id', '=', $this->stylista_id)->first();
        return $stylist['nazwa'];
    }
}