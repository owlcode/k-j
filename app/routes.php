<?php

Route::get('/', function()
{
    return View::make('index');
});

Route::group(array('prefix' => 'api'), function()
{
    Route::get('login', 'BaseController@login');

    Route::get('stylists', 'StylistController@all');
    Route::post('stylists/add', 'StylistController@add');
    Route::delete('stylists/del/{id?}', 'StylistController@delete');

    Route::get('clients', 'ClientController@all');
    Route::get('clients/{id?}', 'ClientController@client');
    Route::delete('clients/del/{id?}', 'ClientController@delete');
    Route::post('clients/add', 'ClientController@add');
    Route::post('clients/edit/{id?}', 'ClientController@edit');
    Route::get('clients/phone/{id?}', 'ClientController@getPhoneNumber');

    Route::get('visits', 'VisitController@all');
    Route::get('visits/userid/{id?}', 'VisitController@userVisits');
    Route::delete('visits/del/{id?}', 'VisitController@delete');
    Route::post('visits/add', 'VisitController@add');
});