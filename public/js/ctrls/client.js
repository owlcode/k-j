client.controller('client', ['$scope', 'Client', '$routeParams', 'Visit', 'Message', 'Stylist', function($scope, Client, $routeParams, Visit, Message, Stylist) {
    $scope.add = false;
    $scope.newVisit = {};

    Stylist.all().success(function(data) {
        $scope.stylists = data;
    });

    Client.info($routeParams.id).success(function(data) {
        $scope.user = data;
    });

    Visit.fromUserId($routeParams.id).success(function(data) {
        $scope.visits = data;
    });

    $scope.deleteVisit = function(id) {
        Message.send('ASK', 'Czy na pewno chcesz usunąć wizytę ?', function() {
            Visit.delete(id).success(function(data) {
                Message.send(data['type'], data['msg']);

                if(data['type'] == 'SUCCESS') {
                    Visit.fromUserId($routeParams.id).success(function(data) {
                        $scope.visits = data;
                    });
                }
            });
        });


    };

    $scope.addVisit = function() {
        $scope.newVisit.clientid = $scope.user.id;

        if(! /[0-9]{4}-[0-9]{2}-[0-9]{2}/.test($scope.newVisit.date))
        {
            Message.send('ERROR', 'Wpisz datę w poprawnym formacie');
            return;
        }

        if ($scope.newVisit.stylistid == '')
        {
            Message.send('ERROR', 'Wybierz stylistę !');
            return;
        }

        if (! /[0-9]{3}/.test($scope.newVisit.price) || ! /[0-9]{2}/.test($scope.newVisit.price))
        {
            Message.send('ERROR', 'Wpisz poprawną cenę !');
            return;
        }

        Visit.add($scope.newVisit).success(function (data) {
            Message.send(data['type'], data['msg']);

            if (data['type'] == 'SUCCESS') {
                Visit.fromUserId($routeParams.id).success(function (data) {
                    $scope.visits = data;
                });

                $scope.add = false;
                $scope.newVisit = {};
            }
        });
    }

    $scope.showPhone = function(id) {
        Message.send('input', 'Podaj hasło', function(pass) {
            if(pass == 'k@sia') {
                Client.phone(id).success(function (data) {
                    Message.send('INFO', [data['name'], data['phone']]);
                });
            } else {
                Message.send('ERROR', 'Blędne hasło !');
            }
        });
    };

    $scope.showVisitForm = function() {
        $scope.add = !$scope.add;
        $scope.newVisit = angular.copy($scope.visits[0]);
        $scope.newVisit.date = '';
        $scope.newVisit.price = '';
    }
}]);