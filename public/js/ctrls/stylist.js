var stylist = angular.module('stylistCtrl', []);

stylist.controller('stylist', ['$scope', 'Stylist', 'Message', function($scope, Stylist, Message) {
    Stylist.all().success(function(data) {
       $scope.stylists = data;
    });

    $scope.showAddForm = function() {
        if($(".top .form").is(':visible')) {
            $(".top .form").fadeOut();
        } else {
            $(".top .form").fadeIn();
        }
    };

    $scope.addStylist = function() {
        Stylist.add($scope.stylist).success(function(data) {

            Message.send(data['type'], data['msg']);

            $scope.stylist = '';


            if(data['type'] == 'SUCCESS') {
                Stylist.all().success(function (data) {
                    $scope.stylists = data;
                })
            }
        });
    };

    $scope.stylistDel = function(id) {
        Stylist.del(id).success(function(data) {

            Message.send(data['type'], data['msg']);

            if(data['type'] == 'SUCCESS') {
                Stylist.all().success(function(data) {
                    $scope.stylists = data;
                });
            }
        });
    };
}]);