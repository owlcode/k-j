var msg = angular.module('messageCtrl', []);

msg.controller('messageShow', ['$scope', 'Message', function($scope, Message) {
   $scope.$on('msg', function(event, args) {
        $scope.args = args;

       if(args[2] == 1) {
           $('.message').fadeIn();
       } else {
           $('.message').fadeIn(500).delay(3000).fadeOut(500);
       }

       $scope.close = function() {
         $('.message').fadeOut();
       };
   });
}]);

msg.controller('askMessage', ['$scope', 'Message', function($scope, Message) {
    $scope.$on('ask', function(event, args) {
        $scope.show = true;
        $scope.question = args[0];

        $scope.confirm = function() {
            args[1]();
            $scope.show = false;
        }

        $scope.reject = function() {
            $scope.show = false;
        }

    });
}]);

msg.controller('inputMessage', ['$scope', 'Message', function($scope, Message) {
    $scope.$on('input', function(event, args) {
        $scope.show = true;
        $scope.question = args[0];

        $scope.confirm = function() {
                args[1]($scope.password);
                $scope.show = false;
                $scope.password = '';
        };

        $scope.reject = function() {
            $scope.show = false;
            $scope.password = '';
        }

    });
}]);