var client = angular.module('clientCtrl', []);

client.controller('clientList', ['$scope', 'Client', 'Message', function($scope, Client, Message) {
    $scope.predicate = 'nazwisko';
    $scope.reverse = false;
    $scope.newClient = {};
    $scope.edit = {};
    $scope.currentlyEditing = false;

    Client.all().success(function(data) {
        $scope.clients = data;
    });

    $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
    };

    $scope.addClient = function() {
        Client.add($scope.newClient).success(function(data) {
            Message.send(data['type'], data['msg']);

            if(data['type'] == 'SUCCESS') {
                Client.all().success(function(data) {
                    $scope.clients = data;

                    $(".clients .add").fadeOut();
                    $scope.newClient = {};
                });
            }
        });
    };

    $scope.deleteClient = function(id) {
        Message.send('ask', 'Czy na pewno chcesz usunąć klienta ?', function() {
            Client.delete(id).success(function(data) {
                Message.send(data['type'], data['msg']);

                if(data['type'] == 'SUCCESS') {
                    Client.all().success(function (data) {
                        $scope.clients = data;
                    });
                }
            });
        });
    };

    $scope.editClient = function(id) {
        if($scope.currentlyEditing == false) {
            $('.edit' + id).hide();
            $('.form' + id).show();

            $scope.currentlyEditing = id;
        } else {
            $scope.edit = {};

            $('.form' + $scope.currentlyEditing).hide();
            $('.edit' + $scope.currentlyEditing).show();

            $('.edit' + id).hide();
            $('.form' + id).show();

            $scope.currentlyEditing = id;
        }
    };

    $scope.editClientSubmit = function(id) {
        Client.edit(id, $scope.edit).success(function(data) {
            Message.send(data['type'], data['msg']);

            if(data['type'] == 'SUCCESS') {
                Client.all().success(function(data) {
                    $scope.clients = data;
                });
            }
        });
    };

    $scope.showPhone = function(id) {
        Message.send('input', 'Podaj hasło', function(pass) {
            if(pass == 'k@sia') {
                Client.phone(id).success(function (data) {
                    Message.send('INFO', [data['name'], data['phone']]);
                });
            } else {
                Message.send('ERROR', 'Blędne hasło !');
            }
        });
    };

    $scope.showAddForm = function () {
        if($('.clients .add').is(':visible')) {
            $(".clients .add").fadeOut();
        } else {
            $(".clients .add").fadeIn();
        }
    };

    $scope.clientLink = function(id) {
        $location.path('/#/klient/'+id);
    };

    $scope.phoneEdit = function(id) {
        if($scope.currentlyEditing == false) {

            $('.clientPhone' + id).hide();
            $('.clientPhoneEdit' + id).show();

            $scope.currentlyEditing = id;
        } else {
            $scope.edit = {};

            $('.clientPhone' + $scope.currentlyEditing).show();
            $('.clientPhoneEdit' + $scope.currentlyEditing).hide();

            $('.clientPhone' + id).hide();
            $('.clientPhoneEdit' + id).show();

            $scope.currentlyEditing = id;
        }
    };

    $scope.clearAll = function() {
        $scope.query = '';
        $scope.predicate = 'id';
        $scope.edit = {};
        $scope.newClient = {};
        $scope.reverse = false;

        if(!$scope.currentlyEditing == false) {
            $('.clientPhone' + $scope.currentlyEditing).show();
            $('.clientPhoneEdit' + $scope.currentlyEditing).hide();
            $scope.currentlyEditing = false;
        }
    };
}]);



