msg.directive('message', function () {
   return {
       restrict: "E",
       templateUrl: "public/views/partials/message.html",
       controller: function($scope, $timeout) {
           $scope.$on('msgEvent', function(event, type, msg, callback) {
               type = type.toLowerCase();
               $scope.msgShow = true;
               $scope.msgType = type;


               if(type == 'info') {
                   $scope.msgTitle = msg[0];
                   $scope.msgText = msg[1];
               } else {
                   $scope.msgText = msg;
               }

                switch(type) {
                   case 'error':
                       $scope.msgIcon = '✘';
                   break;
                   case 'success':
                   case 'info':
                       $scope.msgIcon = '✔';
                   break;
                   case 'ask':
                   case 'input':
                       $scope.msgIcon = '?';
                   break;
               }

                $scope.confirm = function(pass) {
                   $scope.msgShow = false;
                   callback(pass);
               };

               $scope.decline = function() {
                   $scope.msgShow = false;
               };

               if(type == 'error' || type == 'success')
                    $timeout(function (){$scope.msgShow=false;}, 3000);
           });
        }
   };
});