var visitService = angular.module('visitService', []);

visitService.factory('Visit', function($http) {
    return {
        fromUserId : function(id) {
            return $http.get('/kj/api/visits/userid/' + id);
        },
        delete : function(id) {
            return $http.delete('/kj/api/visits/del/' + id);
        },
        add : function(data) {
            return $http.post('/kj/api/visits/add', data);
        }
    }
});