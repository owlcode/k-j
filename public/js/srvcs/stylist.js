var stylistService = angular.module('stylistService', []);

stylistService.factory('Stylist', function($http) {
   return {
       all : function() {
            return $http.get('/kj/api/stylists');
       },
       add : function(data) {
            return $http.post('/kj/api/stylists/add', data);
       },
       del : function(id) {
           return $http.delete('/kj/api/stylists/del/' + id);
       }
   }
});