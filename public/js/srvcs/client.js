var clientService = angular.module('clientService', []);

clientService.factory('Client', function($http) {
   return {
       all : function() {
           return $http.get('/kj/api/clients');
       },
       info : function (id) {
           return $http.get('/kj/api/clients/' + id );
       },
       delete : function (id) {
           return $http.delete('/kj/api/clients/del/' + id );
       },
       add : function(data) {
           return $http.post('/kj/api/clients/add', data);
       },
       edit : function(id, data) {
           return $http.post('/kj/api/clients/edit/' + id, data);
       },
       phone : function(id) {
            return $http.get('/kj/api/clients/phone/' + id);
       }
   }
});