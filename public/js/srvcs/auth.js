angular.module('authService',[])

.factory('Auth', function($http) {
    return {
        login : function (password) {
            return $http.post('/kj/api/login', password);
        },
        isAuth : function () {
            return $http.get('/kj/api/islogged');
        }
    }
});