var messageService = angular.module('messageService', []);

messageService.factory('Message', function($rootScope) {
    return {
        send : function(type, msg, callback) {
            $rootScope.$broadcast('msgEvent', type, msg, callback);
        }
    }
});