angular.module('animations', ['ngAnimate']);

var kj = angular.module('kjApp', [
    'ngRoute',
    'animations',
    'clientCtrl',
    'messageCtrl',
    'stylistCtrl',
    'clientService',
    'stylistService',
    'visitService',
    'messageService',
    'authService'
]);

kj.config(['$routeProvider', function($routeProvider) {
   $routeProvider.
       when('/klienci', {
           templateUrl: 'public/views/klienci.html',
           controller: 'clientList'
       }).
            when('/klient/:id', {
                templateUrl: 'public/views/klient.html',
                controller: 'client'
            }).
       when('/stylisci', {
           templateUrl: 'public/views/stylisci.html',
           controller: 'stylist'
       }).
       when('/wizyty', {
           templateUrl: 'public/views/wizyty.html',
           controller: 'clientList'
       }).
       otherwise({
           redirectTo: '/klienci'
       });
}]);