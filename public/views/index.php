<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
    <script src="public/js/jquery-2.1.4.min.js"></script>
    <script src="public/js/angular.min.js"></script>
    <script src="public/js/angular-animate.js"></script>
    <script src="public/js/angular-route.min.js"></script>

    <script src="public/js/app.js"></script>

    <script src="public/js/ctrls/appCtrl.js"></script>
    <script src="public/js/ctrls/clientList.js"></script>
    <script src="public/js/ctrls/client.js"></script>
    <script src="public/js/ctrls/msg.js"></script>
    <script src="public/js/ctrls/stylist.js"></script>
    <script src="public/js/srvcs/client.js"></script>
    <script src="public/js/srvcs/msgs.js"></script>
    <script src="public/js/srvcs/stylist.js"></script>
    <script src="public/js/srvcs/visit.js"></script>
    <script src="public/js/srvcs/auth.js"></script>
    <script src="public/js/directive/msg.js"></script>


    <link href="public/css/style.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=0.7">
	<title>Salon Fryzjerski KJ</title>
</head>
<body ng-app="kjApp" ng-controller="appCtrl">
<section class="sidebar">
    <header><img src="public/img/kj.jpg" alt="" /></header>
    <nav>
        <ul>
            <li><a href="#/klienci">Klienci</a></li>
            <li><a href="#/wizyty">Wizyty</a></li>
            <li><a href="#/stylisci">Styliści</a></li>
        </ul>
    </nav>
    <footer>owlCode 2015</footer>
</section>
<section class="topbar">
    <header><img src="public/img/kj.jpg" alt="" /></header>
    <a href="#/klienci"><img src="public/img/home.png" alt="" class="home"/></a>
     <a href="#/stylisci"><img src="public/img/scisors.png" alt="" /></a>
</section>
<div class="container">
    <div ng-view class="mainframe"></div>
    <message></message>
</div>
</body>
</html>
